#! /bin/bash



# Get the domain name
echo "Enter domain name (without extension) : "
read domain_name

# Get document root for the domain
echo "Enter document root (absolute path) : "
read document_root


# Append the .local extension to the domain name
local_domain_name="${domain_name}.local"

# ------------------------------

# The target file to write to 
# hosts_file="./test_file"
hosts_file="/etc/hosts"

virtual_host_config_file="/etc/apache2/sites-available/${local_domain_name}.conf"

# ------------------------------


# -------------------------------- 
# Create entry for /etc/hosts file 
#---------------------------------
entry="127.0.0.1 ${local_domain_name}"

# write entry to /etc/hosts file
echo "${entry}" | sudo tee -a $hosts_file >> /dev/null

# -------------------------------- 
# Create virrutal host config file 
#---------------------------------
sites_available_text="<VirtualHost *:80>
	ServerAdmin webmaster@localhost
    ServerName $local_domain_name
    ServerAlias www.${local_domain_name}
	DocumentRoot $document_root
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
"

echo "${sites_available_text}" | sudo tee -a "${virtual_host_config_file}" >> /dev/null

# -------------------------------- 
# Enable the site 
# ---------------------------------
A2ENSITE="/usr/sbin/a2ensite"
sudo ${A2ENSITE} ${local_domain_name} >> /dev/null

# -------------------------------- 
# Reload Apache 
# ---------------------------------
systemctl reload apache2
